"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.scrollbar = void 0;

var scrollbar = function scrollbar() {
  // función para obtener el ancho de la barra de scroll
  var getScrollBarWidth = function getScrollBarWidth() {
    return window.innerWidth - document.documentElement.getBoundingClientRect().width;
  }; // funcion para asignar ese valor a una variable css


  var cssScrollBarWidth = function cssScrollBarWidth() {
    return document.documentElement.style.setProperty('--scrollbar', "".concat(getScrollBarWidth(), "px"));
  }; // asignar la variable css al cargar la página


  addEventListener('load', cssScrollBarWidth); // reasignar la variable css al redimensionar la ventana

  addEventListener('resize', cssScrollBarWidth);
};

exports.scrollbar = scrollbar;